#! /bin/bash


IP=$(hostname -I | awk '{print $2}')
echo "START - install jenkins -"$IP

echo "[1]: install utils & ansible"
sudo apt-get update -qq >/dev/null
sudo apt-get install -qq -y git sshpass wget ansible gnupg2 curl >/dev/null

echo "[2]: install java & jenkins"
sudo wget -qO - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
apt-get update -qq >/dev/null
apt-get install -qq -y default-jre jenkins >/dev/null
/usr/lib/systemd/systemd-sysv-install enable jenkins
#systemctl enable jenkins
systemctl start jenkins

echo "[3]: ansible custom"
# active le pipelining et le allow_world_readable_tmpfiles via le fichier ansible.cfg
sudo sed -i 's/.*pipelining.*/pipelining = True/' /etc/ansible/ansible.cfg
sudo sed -i 's/.*allow_world_readable_tmpfiles.*/allow_world_readable_tmpfiles = True/' /etc/ansible/ansible.cfg

echo "[4]: install docker & docker-composer"
#curl -fsSL https://get.docker.com -o get-docker.sh; >/dev/null
#curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo curl -fsSL https://get.docker.com -o get-docker.sh; >/dev/null
sudo sh get-docker.sh
sudo groupadd docker usermod -aG docker jenkins # authorize docker for jenkins user
curl -sL "https://github.com/docker/compose/release/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose >/dev/null
chmod +x /usr/local/bin/docker-compose

echo "[5]: use registry without ssl"
sudo ssh jenkins-pipeline
echo "{ \"insecure-registries\" : [\"192.168.50.15:5000\"] }" >/etc/docker/daemon.json
systemctl daemon-reload
systemctl restart docker

echo "END - install jenkins"

